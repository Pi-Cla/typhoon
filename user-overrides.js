// Personal Prefs
user_pref("browser.tabs.warnOnClose", true);
user_pref("privacy.resistFingerprinting.letterboxing", false);
user_pref("accessibility.force_disabled", 0);